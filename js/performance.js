function round_off(value,power) {
	var multiplier = Math.pow(10, power);
	return Math.round( value * multiplier ) / multiplier;
}

var abbreviate_number = {
	SI_SYMBOL: ["", "k", "M", "G", "T", "P", "E"],
	handle: function(value){
		var number = value > 0 ? value : Math.abs(value);
		var sign = value > 0 ? '$' : '-$';

	    // what tier? (determines SI symbol)
	    var tier = Math.log10(number) / 3 | 0;

	    // if zero, we don't need a suffix
	    if(tier == 0) return sign + number;

	    // get suffix and determine scale
	    var suffix = abbreviate_number.SI_SYMBOL[tier];
	    var scale = Math.pow(10, tier * 3);

	    // scale the number
	    var scaled = number / scale;

	    // format number and add suffix
	    return sign + scaled.toFixed(1) + suffix;
	}
}

function compute_percentage_to_budget(sales,budget) {
	var percentage = (isFinite(sales / budget) ? sales / budget : 0) * 100;
	return round_off(percentage,0);
}

function compute_figure_budget(sales,budget) {
	var figure = sales - budget;
	return round_off(figure,0);
}

function compute_stly(sales,stly) {
	var percentage = ((isFinite(sales / stly) ? sales / stly : 0) - 1) * 100;
	return round_off(percentage,2);
}

var month = [
	{ short: 'jan', long: 'January' }, { short: 'feb', long: 'February' }, { short: 'mar', long: 'March' },
	{ short: 'apr', long: 'April' }, { short: 'may', long: 'May' }, { short: 'jun', long: 'June' },
	{ short: 'jul', long: 'July' }, { short: 'aug', long: 'August' }, { short: 'sept', long: 'September' },
	{ short: 'oct', long: 'October' }, { short: 'nov', long: 'November' }, { short: 'dec', long: 'December' }
];

var performance = {
	get_data: function() {
		var url = window.location.host == 'localhost' ? 'https://october.mcndigital.com.au/api/v1/fmreport' : 'https://pocket.mcn.com.au/api/v1/fmreport';

		$.ajax({
			url: url,
			data: { API_Freport_Encryption_Key: 'ZVIFLKRQrgX0NgLkc4imVGv89cJwbDhB' }
		})
		.success(function(data) {
			performance.render(data.data);
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
	},
	render: function(data) {
		var units = { tv: {}, digital: {} };
		var names = { tv: [], digital: [] };
		var month_range_reference = [];
		var fiscal_year = [];
		var group = '';
		var progress_props = {
			value: 0,
			animation: false,
			size: 84,
			startAngle: -Math.PI / 4 * 2,
			thickness: 4,
			lineCap: 'butt',
			emptyFill: '#444854',
			fill: {color: '#6cbd50'}	
		};

		$.each(data,function(index, el) {
			group = (el.department_name == 'TV' || el.department_name == 'tv') ? 'tv' : 'digital';	

			if (!units[group].hasOwnProperty(el.channel_group_name)) {
				units[group][el.channel_group_name] = [];
			}

			units[group][el.channel_group_name].push(el);
			if (names[group].indexOf(el.channel_group_name) == -1) names[group].push(el.channel_group_name);
			if (fiscal_year.indexOf(el.fiscal_year) == -1) fiscal_year.push(el.fiscal_year);
			if ($.inArray(el.month - 1,month_range_reference) == -1) month_range_reference.push(el.month - 1);
		});

		function render_cards(group) {
			var html = '';
			$.each(names[group], function(index, name) {
				var cards = '';
				html += '<div class="col" data-group-name="'+ name +'">';
				html += '<h6 class="channel-names text-center">'+ name +'</h6>'
				$.each(units[group][name],function(i, el) {
					var sales = el.sales_revenue, stly = el.sales_revenue_stly, budget = el.budget;

					var computed_figure_budget = compute_figure_budget(sales,budget);
					var abbreviated_figure = abbreviate_number.handle(computed_figure_budget);

					var arrow_class = function() {
						var cssClass = '';
						if (stly < 0) {
							cssClass = 'fa-arrow-minus text-muted static';	
						}
						else {
							cssClass = compute_stly(sales,stly).toString().indexOf('-') != -1 ? 'fa-arrow-down down' : 'fa-arrow-up up';
						}
						return cssClass;
					}

		            cards += '<article class="card card-inverse" data-widget="performance-dashboard">';
					cards += '<div class="card-body performance-dashboard-body">';
		            cards += '<div class="progress-data"><figure class="progress-gauge"><div class="progress-gauge-details">';
		            cards += '<span class="percentage">'+ compute_percentage_to_budget(sales,budget) +'</span>';
		            cards += '<small class="budget">'+ abbreviated_figure +'</small>';
		            cards += '</div><i class="progress-circle" data-value="'+ compute_percentage_to_budget(sales,budget) +'"></i></figure>';

		            stly < 0 ? cards += '<div class="progress-stly"><span class="n-a">N/A</span>' : 
							   cards += '<div class="progress-stly"><span class="percentage">'+ compute_stly(sales,stly) +'</span>';
					cards += '<span><sub>VS</sub> STLY <i class="percentage-indicator-arrows fas '+ arrow_class() +'"></i></span></div></div></div></article>';
				});

				html += cards + '</div>';
			});

			return html;
		}

		var fiscal_year_range = (function() {
			var str = '';
			if (fiscal_year.length == 1) { str = fiscal_year[0]; }
			else { str = fiscal_year[0] + ' - ' + fiscal_year[fiscal_year.length - 1]; }
			return str;
		})();

        var dashboardReportScope = 'MCN '+ fiscal_year_range +' Performance Dashboard <br> <small class="text-muted clearfix">';
        	dashboardReportScope += month[month_range_reference[0]].long+' to '+month[month_range_reference[month_range_reference.length-1]].long;
        	dashboardReportScope += ' Revenue versus Budget and STLY</small>';

		$('#dashboardReportScope').html(dashboardReportScope);

		$('#tv').html('<div class="col-dates dashboardMonths"></div>' + render_cards('tv'));
		$('#digital').html('<div class="col-dates dashboardMonths"></div>' + render_cards('digital'));

		$('.progress-circle').each(function(index, el) {
			var value = parseFloat($(this).data('value')) / 100;
			progress_props.value = value;
			progress_props.fill.color = (function(){
				var hex = '';
				if (value < 0.8) { hex = '#fb3030'; }
				if (value >= 0.8) { hex = '#ff703e'; }
				if (value >= 1) { hex = '#72cb53' }
				return hex;
			})();

			$(this).circleProgress(progress_props);
		});

		var dashboardMonths = '';
		$.each(month_range_reference,function(index, el) {
			dashboardMonths += '<time class="text-uppercase">'+ month[el].short +'</time>';
		});
		$(document).find('.dashboardMonths').each(function(index, el) {
			$(this).html(dashboardMonths);
		});

		// just a little fix to ensure FOX Cricket is always beside FOX Sports
		var anchorForFox = undefined;
		$('#tv').find('[data-group-name]').each(function(index, el) {
			if ($(this).data('group-name').toLowerCase().indexOf('fox sports') !== -1) {
				anchorForFox = $(this);
			}

			if ($(this).data('group-name').toUpperCase() == 'FOX CRICKET' && typeof anchorForFox !== 'undefined') {
				$(this).detach().insertAfter(anchorForFox);
			}
		});

		// Timestamp
		$('#timeStamp').html(moment(data[0]['updated_at']).add(11, 'hours').format('DD MMM YYYY, h:mm a'));
	},
	build: function() {
		this.get_data();
	}
}


$(document).ready(function() {
	performance.build();
});